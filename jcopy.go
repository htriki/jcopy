package main

import (
	"fmt"
	"jcopy/tools"
	"log"
	"os"
	"path/filepath"
)

/*
-----------------------------------------------------------------------------------------------------------------
Main
exemple : go run jcopy.go -in /xxx/in -out=/yyyy/out -log=/zzzz/log.txt -f on
-----------------------------------------------------------------------------------------------------------------
*/
func main() {

	var ut tools.Util

	err := ut.InitArgs()

	if err != nil {
		print(err.Error())
		return
	}

	ut.InitLog()
	ut.DisplayConfiguration()

	ut.TestParamInExist()
	ut.TestParamOutExist()

	if !ut.InError {
		doIt(ut.ParamIn, ut.ParamOut, ut.ParamForce)
	}

	ut.DisplayEnd()
}

/*
-----------------------------------------------------------------------------------------------------------------
Lecture
-----------------------------------------------------------------------------------------------------------------
*/
func doIt(in string, out string, force bool) {

	var lengthIn int = len(in)
	var aDirectory, aFile string

	log.Println("======================================================")
	log.Println("DIR. =", in)

	err := filepath.Walk(in,

		func(path string, info os.FileInfo, err error) error {

			if err != nil {
				return err
			}

			if path != in {

				if info.IsDir() {

					log.Println("======================================================")
					log.Println("DIR. =", path)

					aDirectory = out + path[lengthIn:]
					if _, err := os.Stat(aDirectory); err != nil {
						if os.IsNotExist(err) {
							log.Println(">> non-existent folder")
							os.MkdirAll(aDirectory, os.ModePerm)
						} else {
							// other error
						}
					}
				} else {

					s := fmt.Sprintf("FILE = %s (%d)", path, info.Size())
					log.Println(s)

					aFile = out + path[lengthIn:]
					if file, err := os.Stat(aFile); err != nil {
						if os.IsNotExist(err) {

							log.Println(">> non-existant file")
							copyFile(path, aFile)
						}
					} else {

						if force {

							s, _ := fmt.Println(">> file copied (force)")
							log.Println(s)
							copyFile(path, aFile)

						} else {

							if file.Size() != info.Size() {
								s := fmt.Sprintf(">> file different size (%d / %d)", info.Size(), file.Size())
								log.Println(s)
								copyFile(path, aFile)
							}
						}
					}
				}
			}
			return err
		})

	if err != nil {

		log.Println(err)
	}
}

/*
-----------------------------------------------------------------------------------------------------------------
Copie d'un fichier
-----------------------------------------------------------------------------------------------------------------
*/
func copyFile(path string, aFile string) {

	//input, err := ioutil.ReadFile(path)
	input, err := os.ReadFile(path)
	if err != nil {

		log.Println("Error de création de fichier / lecture ")
		log.Println(err)

	} else {

		//err = ioutil.WriteFile(aFile, input, 0644)
		err = os.WriteFile(aFile, input, 0644)
		if err != nil {

			log.Println("Error de création de fichier / écriture")
			log.Println(err)
		}
	}
}
