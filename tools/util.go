package tools

import (
	"errors"
	"flag"
	"log"
	"os"
	"strconv"
	"strings"
)

type Util struct {
	ParamIn     string
	ParamOut    string
	ParamLog    string
	ParamForce  bool
	InError     bool
	ParamsCount int
}

var FLAGS_ALLOWED = []string{"-in", "-out", "-log", "-f"}

// -----------------------------------------------------------------------------------------------------------------
// Arguments
// -----------------------------------------------------------------------------------------------------------------
func (u *Util) InitArgs() error {

	if isParamExist() {

		var force string

		flag.StringVar(&u.ParamIn, "in", "", "entry directory")
		flag.StringVar(&u.ParamOut, "out", "", "output directory")
		flag.StringVar(&u.ParamLog, "log", "", "log file")
		flag.StringVar(&force, "f", "", "force indicator")
		flag.Parse()

		if force == "on" {
			u.ParamForce = true
		} else {
			u.ParamForce = false
		}

		u.ParamsCount = flag.NFlag()

	} else {

		return errors.New("flag not allowed")
	}

	return nil
}

func isParamExist() bool {

	var isOk = false
	args := os.Args[1:]

	for _, arg := range args {

		if strings.HasPrefix(arg, "-") {

			var exist = false
			for _, f := range FLAGS_ALLOWED {

				if f == arg {
					exist = true
					break
				}
			}
			if !exist {
				isOk = false
				break
			}
		}

		isOk = true
	}

	return isOk
}

// -----------------------------------------------------------------------------------------------------------------
// Définition des log
// -----------------------------------------------------------------------------------------------------------------
func (u *Util) InitLog() {

	if u.ParamLog != "" {

		logFile, err := os.OpenFile(u.ParamLog, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0766)
		if err != nil {
			panic(err)
		}
		log.SetOutput(logFile) //  Définir le fichier à log Fichier de sortie
		log.SetFlags(log.Ldate | log.Ltime)
	}
}

func (u *Util) DisplayConfiguration() {

	log.Println("------------------------------------------------------")
	log.Println("BEGINNIG")
	log.Println("------------------------------------------------------")
	log.Println("in    : " + u.ParamIn)
	log.Println("out   : " + u.ParamOut)
	log.Println("log   : " + u.ParamLog)

	if u.ParamForce {
		log.Println("force : oui")
	} else {
		log.Println("force : non")
	}
	log.Println("nbre  : " + strconv.Itoa(u.ParamsCount))
}

func (u *Util) DisplayEnd() {
	log.Println("------------------------------------------------------")
	log.Println("END")
	log.Println("2021 * Hocine Triki(c)")
	log.Println("------------------------------------------------------")
}

func (u *Util) TestParamInExist() {

	exist := isExist(u.ParamIn)
	if !exist {
		log.Println("entry directory doesn't exist")
		u.InError = true
	}
}

func (u *Util) TestParamOutExist() {
	exist := isExist(u.ParamOut)
	if !exist {
		log.Println("output directory doesn't exist")
		u.InError = true
	}
}

func isExist(path string) bool {

	var isExist bool = false

	if d, err := os.Stat(path); err == nil {
		if d.IsDir() {
			isExist = true
		}
	}
	return isExist
}
